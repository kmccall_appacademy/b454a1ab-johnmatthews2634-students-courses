class Student
  attr_accessor :courses
  attr_reader :first_name, :last_name, :full_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []

  end

  def name

    Student.new(first_name, last_name)
    @full_name = first_name + " " + last_name
  end

  def enroll(course)
    if @courses.include?(course)
    elsif courses.any? {|course_name| course.conflicts_with?(course_name)}
      raise "spaghetti"
    else
      @courses = @courses << course
    end
    course.students << self
  end

  def course_load
    course_hash = {}
    self.courses.each do |course|
      if course_hash[course.department].nil?
        course_hash[course.department] = course.credits
      else
        course_hash[course.department] += course.credits
      end
    end
    course_hash
  end
end
